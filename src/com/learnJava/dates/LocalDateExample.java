package com.learnJava.dates;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class LocalDateExample {

    public static void main(String[] args) {

        LocalDate localDate = LocalDate.of(2018, 12, 30);
        LocalDate localDate3 = LocalDate.of(2018, Month.JULY, 28);
        LocalDate localDate2 = LocalDate.ofYearDay(2018, 365);
        //LocalDate localDate3 = LocalDate.ofEpochDay(1660618621);
        System.out.println(localDate);
        System.out.println(localDate3);
        System.out.println(localDate2);
        //System.out.println(localDate3);


        //Modifying LocalDate
        System.out.println("Add months==>"+localDate.plusMonths(2));
        System.out.println("Add Days==>"+localDate.plusDays(2));
        System.out.println("Add Years==>"+localDate.plusYears(2));
        System.out.println("Minus month==>"+localDate.minusMonths(2));
        System.out.println("Minus Days==>"+localDate.minusDays(2));
        System.out.println("Minus Years==>"+localDate.minusYears(2));

        LocalDate localDate1 = localDate.plusDays(1);
        System.out.println("Check 1:"+localDate.isAfter(localDate1));
        System.out.println("Check 2:"+localDate.isBefore(localDate1));
        System.out.println("Check 3:"+localDate.isLeapYear());
        System.out.println("Check 4:"+localDate.isEqual(localDate1));
        System.out.println("Check 5:"+localDate.isSupported(ChronoUnit.MINUTES));
    }
}
