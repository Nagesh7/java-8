package com.learnJava.dates;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeExample {

    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();
        LocalTime localTime1 = LocalTime.of(12, 45);
        System.out.println("Time 1:"+localTime+" Time 2:"+localTime1);
        System.out.println("Hour:"+localTime.getHour());
        System.out.println("Minute:"+localTime.getMinute());
        System.out.println("Seconds:"+localTime.getSecond());
        System.out.println("Time in Seconds:"+localTime.toSecondOfDay());

        //Modifying LocalDate
        System.out.println("Add Hours==>"+localTime.plusHours(2));
        System.out.println("Add Minutes==>"+localTime.plusMinutes(2));
        System.out.println("Add Seconds==>"+localTime.plusSeconds(2));
        System.out.println("Minus Hours==>"+localTime.minusHours(2));
        System.out.println("Minus Minutes==>"+localTime.minusMinutes(2));
        System.out.println("Minus Hours==>"+localTime.minusSeconds(2));

        LocalTime localTime2 = localTime.plusHours(1);
        System.out.println("Check 1:"+localTime.isAfter(localTime2));
        System.out.println("Check 2:"+localTime.isBefore(localTime2));
        System.out.println("Check 5:"+localTime.isSupported(ChronoUnit.MINUTES));

    }
}
