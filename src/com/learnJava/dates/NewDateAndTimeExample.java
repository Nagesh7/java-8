package com.learnJava.dates;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class NewDateAndTimeExample {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("Local Date:"+localDate+" Local Time:"+localTime+" Local Date & Time:"+localDateTime);
        System.out.println("LocalDate to LocalDateTime:"+localDate.atTime(12, 12));
        System.out.println("LocalTime to LocalDateTime:"+localTime.atDate(localDate));
        System.out.println("LocalDateTime to LocalDate:"+localDateTime.toLocalDate());
        System.out.println("LocalDateTime to LocalTime:"+localDateTime.toLocalTime());




    }
}
