package com.learnJava.defaults;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DefaultMethodExample {
    public static void main(String[] args) {
        List<String> nameList = Arrays.asList("Bob", "Mike", "Anna", "Alwin", "Savleen");
        /**
         * Before Java 8
         */
        Collections.sort(nameList);
        System.out.println(nameList);

        /**
         * After Java 8
         */
        nameList.sort(Comparator.naturalOrder());
        System.out.println(nameList);
        nameList.sort(Comparator.reverseOrder());
        System.out.println(nameList);
    }
}
