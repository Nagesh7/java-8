package com.learnJava.defaults;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class DefaultMethodExample2 {

    private static Consumer<Student> studentConsumer = System.out::println;
    private static Comparator<Student> nameComparator = Comparator.comparing(Student::getName);
    private static Comparator<Student> gpaComparator = Comparator.comparingDouble(Student::getGpa);

    private static void sortByName(List<Student> studentList) {
        studentList.sort(nameComparator);
        System.out.println("Sorting based on Name:"+studentConsumer);
    }

    private static void sortByNameAndGPA(List<Student> studentList) {
        studentList.sort(nameComparator.thenComparing(gpaComparator));
        System.out.println("Sorting based on Name & GPA:"+studentConsumer);
    }

    public static void main(String[] args) {
        List<Student> studentList = StudentDataBase.getAllStudents();
        sortByName(studentList);
        sortByNameAndGPA(studentList);
    }
}
