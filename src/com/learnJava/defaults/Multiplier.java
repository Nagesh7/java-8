package com.learnJava.defaults;

import java.util.List;

public interface Multiplier {

    int multiply(List<Integer> integerList);

    /**
     * We can override default methods but it's optional
     * @param integerList
     * @return
     */
    default int size(List<Integer> integerList) {
        return integerList.size();
    }

    static boolean isEmpty(List<Integer> integerList) {
        return integerList.isEmpty();
    }
}
