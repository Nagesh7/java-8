package com.learnJava.defaults;

import java.util.Arrays;
import java.util.List;

public class MultiplierExample {

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(4, 5, 9, 2);
        MultiplierImpl multiplier = new MultiplierImpl();
        System.out.println("Answer:"+multiplier.multiply(integerList));
        System.out.println("Size:"+multiplier.size(integerList));
        System.out.println("Size:"+Multiplier.isEmpty(integerList));
    }
}
