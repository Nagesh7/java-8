package com.learnJava.functionalInterfaces;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.function.BiConsumer;

public class BiConsumerExample {


    public static void printNameAndActivities() {
        BiConsumer<String, List<String>> biConsumer = (name, activities) -> System.out.println("Name : "+name+" & Activities : "+activities);
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(student -> biConsumer.accept(student.getName(), student.getActivities()));

    }

    public static void main(String[] args) {

        BiConsumer<String, String> biConsumer = (str1, str2) -> System.out.println("a: " + str1 + " b: " + str2);

        biConsumer.accept("Java 7", "Java 8");

        BiConsumer<Integer, Integer> addition = (val1, val2) -> System.out.println("Addition Result :"+(val1+val1));

        BiConsumer<Integer, Integer> subtraction = (val1, val2) -> System.out.println("Subtraction Result :"+(val1-val2));

        BiConsumer<Integer, Integer> multiplication = (val1, val2) -> System.out.println("Multiplication Result :"+(val1*val2));

        BiConsumer<Integer, Integer> division = (val1, val2) -> System.out.println("Division Result :"+(val1/val2));

        addition.andThen(subtraction).andThen(multiplication).andThen(division).accept(6,2);

        printNameAndActivities();
    }
}
