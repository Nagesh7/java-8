package com.learnJava.functionalInterfaces;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

    static Consumer<Student> studentConsumer = (student) -> System.out.println(student);
    static Consumer<Student> studentNameConsumer = (student) -> System.out.print(student.getName());
    static Consumer<Student> studentActivitiesConsumer = (student) -> System.out.println(student.getActivities());

    public static void printStudents() {
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(studentConsumer);
    }

    public static void printNameAndActivities() {
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(studentNameConsumer.andThen(studentActivitiesConsumer));
    }

    public static void printNameAndActivitiesBasedOnCondition() {
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(student -> {
            if(student.getGradeLevel() > 3)
                studentNameConsumer.andThen(studentActivitiesConsumer).accept(student);
        });
    }

    public static void main(String[] args) {
        Consumer<String> c1 = (str) -> System.out.println(str.toUpperCase());
        c1.accept("Hello");
        printStudents();
        printNameAndActivities();
        printNameAndActivitiesBasedOnCondition();
    }

}
