package com.learnJava.functionalInterfaces;

import java.util.function.Function;

public class FunctionExample {
    static Function<String, String> function = (name) -> name.toUpperCase();
    static Function<String, String> concatFunction = (name) -> name.concat(" Default ");

    public static void main(String[] args) {
        System.out.println("Function output ==>"+function.apply("java 8"));
        System.out.println("andThen output ===>"+function.andThen(concatFunction).apply("java 8"));
        System.out.println("compose output ===>"+function.compose(concatFunction).apply("java 8"));
    }
}
