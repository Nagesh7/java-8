package com.learnJava.functionalInterfaces;

import java.util.function.IntPredicate;
import java.util.function.Predicate;

public class PredicateExample {

    static IntPredicate predicate = (val) -> val%2==0;
    static IntPredicate predicate1 = (val) -> val%5==0;

    public static void predicateAnd() {
        System.out.println("Predicate and result is : "+ predicate.and(predicate1).test(10));
        System.out.println("Predicate and result is : "+ predicate.and(predicate1).test(9));

    }

    public static void predicateOr() {
        System.out.println("Predicate OR result is : "+ predicate.or(predicate1).test(10));
        System.out.println("Predicate OR result is : "+ predicate.or(predicate1).test(8));

    }

    public static void predicateNegate() {
        System.out.println("Predicate Negate result is : "+ predicate.negate().test(10));
        System.out.println("Predicate Negate result is : "+ predicate.negate().test(5));

    }

    public static void main(String[] args) {
        System.out.println(predicate.test(8));
        predicateAnd();
        predicateOr();
        predicateNegate();
    }
}
