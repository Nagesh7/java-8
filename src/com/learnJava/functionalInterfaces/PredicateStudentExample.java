package com.learnJava.functionalInterfaces;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.function.Predicate;

public class PredicateStudentExample {

    static Predicate<Student> gradeFilter = (student) -> student.getGradeLevel() >= 3;
    static Predicate<Student> gpaFilter = (student) -> student.getGpa() > 3.8;

    public static void filterStudentsByGradeAndGpa() {
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(student -> {
            if(gradeFilter.and(gpaFilter).test(student))
                System.out.println("Filtered Data : "+student);
        });
    }

    public static void negateFilteredStudentsDataByGradeAndGpa() {
        List<Student> studentList = StudentDataBase.getAllStudents();
        studentList.forEach(student -> {
            if(gradeFilter.and(gpaFilter).negate().test(student))
                System.out.println("Negated Data : "+student);
            else
                System.out.println("Students Data : "+student);
        });
    }

    public static void main(String[] args) {
        filterStudentsByGradeAndGpa();
        negateFilteredStudentsDataByGradeAndGpa();
    }
}
