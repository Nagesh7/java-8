package com.learnJava.lambdas;

import java.util.Comparator;

public class ComparatorLambdaExample {

    public static void main(String[] args) {


        /**
         * Prior Java 8
         */
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        System.out.println("The result of comparator is:"+comparator.compare(3,4));


        /**
         * Java 8 - Lambda
         */
        Comparator<Integer> lambdaComparator = (Integer a, Integer b) -> a.compareTo(b);
        System.out.println("The result of comparator using Lambda is:"+lambdaComparator.compare(5,4));

        /**
         * Java 8 - Lambda without type in parameter
         */
        Comparator<Integer> lambdaComparator1 = (a, b) -> a.compareTo(b);
        System.out.println("The result of comparator using Lambda is:"+lambdaComparator1.compare(5,5));

    }
}
