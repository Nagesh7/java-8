package com.learnJava.lambdas;

public class RunnableLambdaExample {
    public static void main(String[] args) {

        /**
         * Prior Java 8
         */
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Inside runnable");
            }
        };
        new Thread(runnable).start();


        /**
         * Java 8 - Lambda
         */
        //Note: We can avoid curly braces if we have only single line in lambda body.
        Runnable lambdaRunnable = () ->{
            System.out.println("Inside Lambda Runnable");
        };
        new Thread(lambdaRunnable).start();



        /**
         * Java 8 - Passing lambda expression as parameter for Thread class
         */
        new Thread(()-> System.out.println("Parameterized Lambda runnable")).start();
    }
}
