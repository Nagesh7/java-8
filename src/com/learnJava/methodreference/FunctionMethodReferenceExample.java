package com.learnJava.methodreference;

import java.util.function.Function;

public class FunctionMethodReferenceExample {

    static Function<String, String> function = (s)->s.toUpperCase();
    static Function<String, String> methodReferenceFunction = String::toUpperCase;

    public static void main(String[] args) {
        System.out.println("Function ==>"+function.apply("Java 8"));
        System.out.println("Method Reference Function ==>"+methodReferenceFunction.apply("Java 8"));
    }
}
