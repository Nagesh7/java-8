package com.learnJava.numericstreams;

import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.OptionalLong;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class NumericStreamsAggregatorExample {

    public static void main(String[] args) {
        int sum = IntStream.range(1,50).sum();
        System.out.println("Sum:"+sum);

        OptionalInt min = IntStream.range(1, 50).min();
        System.out.println(min.isPresent()?min.getAsInt():Integer.MIN_VALUE);

        OptionalLong max = LongStream.range(1, 50).max();
        System.out.println(max.isPresent()?max.getAsLong():Long.MAX_VALUE);

        OptionalDouble average = LongStream.range(1, 50).asDoubleStream().average();
        System.out.println(average.isPresent()?average.getAsDouble():0.0);

    }
}
