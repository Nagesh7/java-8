package com.learnJava.numericstreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumericStreamsBoxingUnBoxingExample {

    public static List<Integer> boxing() {
        return IntStream.rangeClosed(1, 50)
                .boxed()
                .collect(Collectors.toList());
    }

    public static long unBoxing(List<Integer> integerList) {
        return integerList.stream()
                .mapToInt(Integer::intValue)
                .sum();

/*        return integerList.stream()
                .mapToLong(x->x)
                .sum();

        List<Long> longList = Arrays.asList(1l,2l,3l,4l,5l,6l);
        return longList.stream()
                .mapToLong(Long::longValue)
                .sum();*/

    }

    public static void main(String[] args) {
        List<Integer> integerList = boxing();
        System.out.println("Boxing :"+integerList);
        System.out.println("UnBoxing:"+unBoxing(integerList));
    }
}
