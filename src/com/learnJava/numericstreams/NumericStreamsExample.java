package com.learnJava.numericstreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class NumericStreamsExample {


    public static int sumOfIntegerListUsingStream(List<Integer> integerList) {
        return integerList.stream()
                .reduce(0, Integer::sum); //Unboxing will happen here.
    }

    public static int sumUsingNumericStream() {
        return IntStream.rangeClosed(1, 6)
                .sum();
    }


        public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1,2,3,4,5,6);
        System.out.println("Sum using stream():"+sumOfIntegerListUsingStream(integerList));
        System.out.println("Sum Using Numeric Stream:"+sumUsingNumericStream());
    }
}
