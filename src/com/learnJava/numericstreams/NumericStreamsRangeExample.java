package com.learnJava.numericstreams;

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class NumericStreamsRangeExample {

    public static void main(String[] args) {
        IntStream.range(1,50).forEach(num -> System.out.println("IntStream.range(1,50)===>"+num));
        IntStream.rangeClosed(1,50).forEach(num -> System.out.println("IntStream.rangeClosed(1,50)===>"+num));

        LongStream.range(1,50).forEach(num -> System.out.println("LongStream.range(1,50)===>"+num));
        LongStream.rangeClosed(1,50).forEach(num -> System.out.println("LongStream.rangeClosed(1,50)===>"+num));

        IntStream.range(1,50).asDoubleStream().forEach(num -> System.out.println("IntStream.range(1,50).asDoubleStream()===>"+num));
    }
}
