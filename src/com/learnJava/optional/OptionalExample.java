package com.learnJava.optional;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class OptionalExample {

    public static Optional<String> getStudentNameOptional() {
        Optional<Student> studentOptional = Optional.ofNullable(StudentDataBase.studentSupplier.get());
        if(studentOptional.isPresent()) {
            return studentOptional.map(Student::getName);
        }
        return Optional.empty();
    }

    public static void main(String[] args) {

        Optional<String> studentNameOptional = getStudentNameOptional();
        if(studentNameOptional.isPresent())
            System.out.println("Student Name length:"+studentNameOptional.get());
        else
            System.out.println("Student is not found");
    }
}
