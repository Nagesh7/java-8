package com.learnJava.optional;

import java.util.Optional;

public class OptionalOfEmptyNullableExample {

    public static Optional<String> ofNullable() {
        return Optional.ofNullable("Hello"); // Optional.ofNullable() Accepts null also.
    }

    public static Optional<String> of() {
        return Optional.of("Nagesh"); //Optional.of() doesn't accepts null as a parameter
    }

    public static Optional<String> empty() {
        return Optional.empty();
    }

    public static void main(String[] args) {
        Optional<String> optional = ofNullable();
        System.out.println("ofNullable:"+optional);
        optional = of();
        System.out.println("of:"+optional);
        optional = empty();
        System.out.println("empty:"+optional);
    }
}
