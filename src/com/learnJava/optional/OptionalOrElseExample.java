package com.learnJava.optional;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class OptionalOrElseExample {

    public static String orElse() {
        Optional<Student> studentOptional = Optional.ofNullable(StudentDataBase.studentSupplier.get());
        return studentOptional.map(Student::getName).orElse("Default");
    }

    public static String orElseGet() {
        Optional<Student> studentOptional = Optional.ofNullable(StudentDataBase.studentSupplier.get());
        return studentOptional.map(Student::getName).orElseGet(()->"Default");
    }

    public static String orElseThrow() {
        Optional<Student> studentOptional = Optional.ofNullable(StudentDataBase.studentSupplier.get());
        return studentOptional.map(Student::getName).orElseThrow(()-> new RuntimeException("Data not found"));
    }

    public static void main(String[] args) {
        System.out.println("orElse():"+orElse());
        System.out.println("orElseGet():"+orElseGet());
        System.out.println("orElseThrow():"+orElseThrow());
    }
}
