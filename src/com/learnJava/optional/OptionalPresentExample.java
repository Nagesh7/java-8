package com.learnJava.optional;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import javax.swing.text.html.Option;
import java.util.Optional;

public class OptionalPresentExample {

    public static void main(String[] args) {
        Optional<String> optionalName = Optional.ofNullable(StudentDataBase.studentSupplier.get())
                .map(Student::getName);
        if(optionalName.isPresent())
            System.out.println("Name:"+optionalName.get());

        optionalName.ifPresent(name -> {
            String updatedName = name+" Singh";
            System.out.println("Updated Name:"+updatedName);
        });
    }
}
