package com.learnJava.streams;

import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.stream.IntStream;

public class ParallelStreamsExample {


    public static void printStudentNames() {
        StudentDataBase.getAllStudents().parallelStream()
                .forEach(System.out::println);
    }

    public static void printNumbers() {
        IntStream.rangeClosed(1,100).parallel()
                .forEach(System.out::println);
    }
    public static void main(String[] args) {
        printStudentNames();
        printNumbers();

    }
}
