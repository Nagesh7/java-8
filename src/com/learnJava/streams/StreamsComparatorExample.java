package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsComparatorExample {

    public static List<Student> sortStudentByName() {
        return StudentDataBase.getAllStudents().stream()
                .sorted(Comparator.comparing(Student::getName))
                .collect(Collectors.toList());
    }

    public static List<Student> sortStudentByGPA() {
        return StudentDataBase.getAllStudents().stream()
                .sorted(Comparator.comparing(Student::getGpa))
                .collect(Collectors.toList());
    }

    public static List<Student> sortStudentByGPAInDescending() {
        return StudentDataBase.getAllStudents().stream()
                .sorted(Comparator.comparing(Student::getGpa).reversed())
                .collect(Collectors.toList());
    }

    public static List<Student> sortStudentByNameAndGPA() {
        return StudentDataBase.getAllStudents().stream()
                .sorted(Comparator.comparing(Student::getName).thenComparing(Student::getGpa))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println("Students Sorted based on Name: ");
        sortStudentByName().forEach(System.out::println);

        System.out.println("Students Sorted based on GPA: ");
        sortStudentByGPA().forEach(System.out::println);

        System.out.println("Students Sorted based on GPA Descending: ");
        sortStudentByGPAInDescending().forEach(System.out::println);

        System.out.println("Students Sorted based on Name & GPA: ");
        sortStudentByGPAInDescending().forEach(System.out::println);
    }
}
