package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamsExample {

    public static void main(String[] args) {

        Predicate<Student> gradeLevelPredicate = (student) -> student.getGradeLevel() >=3;
        Predicate<Student> gpaPredicate = (student) -> student.getGpa() >=4;
        Consumer<Student> studentConsumer = System.out::println;

        Map<String, List<String>> studentMap = StudentDataBase.getAllStudents().stream()
                .peek(studentConsumer)
                .filter(gradeLevelPredicate.and(gpaPredicate))
                .peek(student -> System.out.println("After Filter===>"+student))
                .collect(Collectors.toMap(Student::getName, Student::getActivities));
        System.out.println(studentMap);
    }
}
