package com.learnJava.streams;

import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamsFactoryMethodsExample {

    public static void main(String[] args) {

        System.out.println("Stream.of() Result ===>");
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        integerStream.forEach(System.out::println);

        System.out.println("Stream.iterator() Result ===>");
        Stream.iterate(1, x -> x*2)
                .limit(10)
                .forEach(System.out::println);


        System.out.println("Stream.generate() Result ===>");
        Supplier<Integer> integerSupplier = new Random()::nextInt;
        Stream.generate(integerSupplier)
                .limit(10)
                .forEach(System.out::println);
    }
}
