package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class StreamsFindExample {

    public static Optional<Student> findAny() {
        return StudentDataBase.getAllStudents().stream()
                .filter(student -> student.getGpa() >3.9)
                .findAny();
    }

    public static Optional<Student> findFirst() {
        return StudentDataBase.getAllStudents().stream()
                .filter(student -> student.getGpa() >3.9)
                .findFirst();
    }

    public static void main(String[] args) {

        Optional<Student> findAny = findAny();
        findAny.ifPresent(System.out::println);

        Optional<Student> findFirst = findFirst();
        findFirst.ifPresent(System.out::println);
    }
}
