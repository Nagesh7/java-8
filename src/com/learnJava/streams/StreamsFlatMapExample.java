package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.List;
import java.util.stream.Collectors;

public class StreamsFlatMapExample {

    public static List<String> getAllStudentActivities() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getActivities)  //Stream<List<String>>
                .flatMap(List::stream)  //Stream<String>
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static long getAllStudentActivitiesCount() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getActivities)  //Stream<List<String>>
                .flatMap(List::stream)  //Stream<String>
                .distinct()
                .count();
    }

    public static void main(String[] args) {
        System.out.println(getAllStudentActivities());
        System.out.println(getAllStudentActivitiesCount());
    }
}
