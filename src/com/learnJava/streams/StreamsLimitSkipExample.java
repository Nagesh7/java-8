package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class StreamsLimitSkipExample {

    public static Optional<Integer> getStudentNoteBooksCountLimit() {
        return StudentDataBase.getAllStudents().stream()
                .limit(3)
                .map(Student::getNoteBooks)
                .reduce(Integer::sum);
    }

    public static Optional<Integer> getStudentNoteBooksCountSkip() {
        return StudentDataBase.getAllStudents().stream()
                .skip(3)
                .map(Student::getNoteBooks)
                .reduce(Integer::sum);
    }

    public static void main(String[] args) {
        Optional<Integer> limitSum = getStudentNoteBooksCountLimit();
        limitSum.ifPresent(integer -> System.out.println("Sum with Limit===>"+integer));

        Optional<Integer> skipSum = getStudentNoteBooksCountSkip();
        skipSum.ifPresent(integer -> System.out.println("Sum with Skip===>"+integer));
    }
}
