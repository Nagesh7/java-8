package com.learnJava.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamsReduceExample {

    public static int multiplyListData(List<Integer> integerList) {
        return integerList.stream()
                .reduce(1, (a, b) -> a*b);
    }

    public static Optional<Integer> multiplyListDataWithoutIdentity(List<Integer> integerList) {
        return integerList.stream()
                .reduce((a, b) -> a*b);
    }

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1,2,3,4,5);
        System.out.println("Multiplication result ===>"+multiplyListData(integerList));
        Optional<Integer> multiplicationResult = multiplyListDataWithoutIdentity(integerList);
        multiplicationResult.ifPresent(System.out::println);
    }
}
