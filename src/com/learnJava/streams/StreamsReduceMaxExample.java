package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class StreamsReduceMaxExample {

    public static Integer getStudentNoteBooksCount() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(0, Integer::max);
    }

    public static Optional<Integer> getStudentNoteBooksCountOptional() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(Integer::max);
    }

    public static void main(String[] args) {
        System.out.println("Max of NoteBooks ===>"+getStudentNoteBooksCount());
        Optional<Integer> max = getStudentNoteBooksCountOptional();
        max.ifPresent(System.out::println);
    }
}
