package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.Optional;

public class StreamsReduceMinExample {

    public static Integer getStudentNoteBooksCount() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(0, Integer::min);
    }

    public static Optional<Integer> getStudentNoteBooksCountOptional() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(Integer::min);
    }

    public static void main(String[] args) {
        System.out.println("Min of NoteBooks ===>"+getStudentNoteBooksCount());
        Optional<Integer> min = getStudentNoteBooksCountOptional();
        min.ifPresent(System.out::println);
    }
}
