package com.learnJava.streams;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import javax.swing.text.html.Option;
import java.util.Optional;

public class StreamsReduceSumExample {

    public static Integer getStudentNoteBooksCount() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(0, Integer::sum);
    }

    public static Optional<Integer> getStudentNoteBooksCountOptional() {
        return StudentDataBase.getAllStudents().stream()
                .map(Student::getNoteBooks)
                .reduce(Integer::sum);
    }

    public static void main(String[] args) {
        System.out.println("Sum of NoteBooks ===>"+getStudentNoteBooksCount());
        Optional<Integer> sum = getStudentNoteBooksCountOptional();
        sum.ifPresent(System.out::println);
    }
}
