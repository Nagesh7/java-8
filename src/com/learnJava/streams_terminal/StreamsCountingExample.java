package com.learnJava.streams_terminal;

import com.learnJava.data.StudentDataBase;

import java.util.stream.Collectors;

public class StreamsCountingExample {

    public static long counting() {
        return StudentDataBase.getAllStudents().stream()
                .collect(Collectors.counting());
    }

    public static void main(String[] args) {
        System.out.println("Count:"+counting());
    }
}
