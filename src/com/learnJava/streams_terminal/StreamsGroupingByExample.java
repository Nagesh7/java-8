package com.learnJava.streams_terminal;

import com.learnJava.data.Student;
import com.learnJava.data.StudentDataBase;

import java.util.*;
import java.util.stream.Collectors;

public class StreamsGroupingByExample {

    public static void groupingByGender() {
        Map<String, List<Student>> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(Student::getGender));
        System.out.println(map);
    }

    public static void groupingByGenderCustomized() {
        Map<String, List<Student>> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(student -> student.getGpa() > 3.8?"EXCELLENT":"AVERAGE"));
        System.out.println(map);
    }

    public static void twoLevelGrouping_1() {
        Map<String, Long> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(Student::getGender, Collectors.counting()));
        System.out.println(map);
    }

    public static void twoLevelGrouping_2() {
        Map<String, Map<Integer, List<Student>>> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(Student::getGender, Collectors.groupingBy(Student::getNoteBooks)));
        System.out.println(map);
    }

    public static void threeLevelGrouping_1() {
        LinkedHashMap<String, Set<Student>> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(Student::getGender, LinkedHashMap::new, Collectors.toSet()));
        System.out.println(map);
    }

    public static void groupingByTopGPA() {
        Map<String, Optional<Student>> map = StudentDataBase.getAllStudents().stream()
                .collect(Collectors.groupingBy(Student::getGender, Collectors.maxBy(Comparator.comparing(Student::getGpa))));
        System.out.println(map);
    }

    public static void main(String[] args) {
        groupingByGender();
        groupingByGenderCustomized();
        twoLevelGrouping_1();
        twoLevelGrouping_2();
        threeLevelGrouping_1();
        groupingByTopGPA();

    }
}
